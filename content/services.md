+++
title = "What we do?"
slug = "services"
+++

Our main activity is working on our free software projects or contribute to others. Ocasionally we provide the following consulting services for people and organizations:

* Development of tailored applications for specific requirements;
* Support and education on how to use specific free software;
* Implement free software in replacement of proprietary ones.