---
title: "Reverse engineering Youblisher"
date: 2018-06-06T01:06:36+01:00
---

Youblisher is a publishing platform that allows users to upload PDFs and turn them into "flipbooks". The platform is nice, but sometimes what we need is really a PDF and not a fancy flipbook, so let's work on that.

As a mere example, I'll use [this document](http://www.youblisher.com/p/1947807-La-Bustia-edicio-maig-2018/) as a reference throughout the post.

When you open the document, you'll see a nice UI with the flipbook, navigation controls and information like number of pages, which in this particular case is *32*.

To make it easier to analyse, you can download the page using `curl`:

{{< highlight bash >}}
$ curl http://www.youblisher.com/p/1947807-La-Bustia-edicio-maig-2018/ > document.html
{{< / highlight >}}

The first question that pops out is how the hell we're gonna turn this nice flipbook into a PDF. If we look at the network requests being made when the page is loaded, we'll notice that each page of the flipbook is in fact an image. This makes it much easier to create the PDF as we can use a simple image per page and that's it.

Inspecting the [page source code](view-source:http://www.youblisher.com/p/1947807-La-Bustia-edicio-maig-2018/) (`CTRL+U` in most browsers) reveals useful information to fetch each document's page or image. From L471 to L504:

{{< highlight go "linenos=table,linenostart=471" >}}
var imagePath = "http://static.youblisher.com/publications/325/1947807/large-";
var flashpages = {
1: {file: '1947807-1.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
2: {file: '1947807-2.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
3: {file: '1947807-3.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
4: {file: '1947807-4.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
5: {file: '1947807-5.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
6: {file: '1947807-6.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
7: {file: '1947807-7.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
8: {file: '1947807-8.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
9: {file: '1947807-9.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
10: {file: '1947807-10.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
11: {file: '1947807-11.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
12: {file: '1947807-12.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
13: {file: '1947807-13.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
14: {file: '1947807-14.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
15: {file: '1947807-15.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
16: {file: '1947807-16.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
17: {file: '1947807-17.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
18: {file: '1947807-18.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
19: {file: '1947807-19.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
20: {file: '1947807-20.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
21: {file: '1947807-21.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
22: {file: '1947807-22.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
23: {file: '1947807-23.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
24: {file: '1947807-24.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
25: {file: '1947807-25.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
26: {file: '1947807-26.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
27: {file: '1947807-27.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
28: {file: '1947807-28.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
29: {file: '1947807-29.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
30: {file: '1947807-30.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
31: {file: '1947807-31.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' },
32: {file: '1947807-32.jpg', wide: 'n', width: '1600', height: '2152', version: 'jpg' }};
{{< / highlight >}}

Basically the above lines give us all the information we need to rebuild a PDF from the flipbook. Now that we know how to do it, lets write a small tool to automate it.

## Unblisher

We'll use [go](https://golang.org) to create `unblisher` &mdash; a CLI tool that generates a PDF from a Youblisher flipbook. There are 3 steps required to achieve this:

1. Fetch the document's images information (we already saw where we can get this information above);
2. Download all images;
3. Generate a PDF and write it to disk.

Simple enough.

##### `unblisher/core/document.go`

{{< highlight go "linenos=table" >}}
package core

import (
	"bytes"
	"regexp"
	"strings"

	"github.com/jung-kurt/gofpdf"
)

type DocumentImage struct {
	Name    string
	Content []byte
	URL     string
}

func (img *DocumentImage) Download() error {
	content, err := httpGet(img.URL)
	if err != nil {
		return err
	}

	img.Content = content

	return nil
}

type Document struct {
	Images     []*DocumentImage
	OutputPath string

	url string
}

func NewDocument(url, outputPath string) (*Document, error) {
	doc := &Document{OutputPath: outputPath, url: url}

	if err := doc.FetchInfo(); err != nil {
		return nil, err
	}

	return doc, nil
}

// FetchInfo downloads the page and tries
// to find the images' names and base URL.
func (doc *Document) FetchInfo() error {
	raw, err := httpGet(doc.url)
	if err != nil {
		return err
	}

	imagesBaseURL := findImagesBaseURL(string(raw))
	imagesNames := findImagesNames(string(raw))

	for _, imgName := range imagesNames {
		img := &DocumentImage{
			Name: imgName,
			URL:  imagesBaseURL + imgName,
		}

		doc.Images = append(doc.Images, img)
	}

	return nil
}

// GeneratePDF will create a new PDF with a
// page for each image in `.Images`. The final PDF is written
// to `outputPath`.
func (doc *Document) GeneratePDF() error {
	// Init PDF
	pdf := gofpdf.New("P", "mm", "A4", "")

	// Add pages with images
	for _, img := range doc.Images {
		pagew, pageh := pdf.GetPageSize()

		pdf.AddPage()
		pdf.RegisterImageReader(img.Name, "jpg", bytes.NewReader(img.Content))
		pdf.Image(img.Name, 0, 0, pagew, pageh, false, "", 0, "")
	}

	// Write file and close
	return pdf.OutputFileAndClose(doc.OutputPath)
}

func findImagesBaseURL(raw string) string {
	re := regexp.MustCompile("imagePath = \"(.|\n)*?\"")
	url := re.FindAllString(raw, -1)[0]
	url = strings.Replace(url, "imagePath = \"", "", -1)
	url = strings.Replace(url, "\"", "", -1)

	return url
}

func findImagesNames(raw string) []string {
	re := regexp.MustCompile("file: '(.|\n)*?'")
	result := re.FindAllString(raw, -1)
	images := []string{}

	for _, f := range result {
		f = strings.Replace(f, "file: '", "", -1)
		f = strings.Replace(f, "'", "", -1)

		images = append(images, f)
	}

	return images
}
{{< / highlight >}}

##### `unblisher/core/util.go`

{{< highlight go "linenos=table" >}}
package core

import (
	"io/ioutil"
	"net/http"
)

func httpGet(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}
{{< / highlight >}}

##### `unblisher/cmd/unblisher.go`

{{< highlight go "linenos=table" >}}
package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/und0ck3d/unblisher/core"
)

var unblisherCmd = &cobra.Command{
	Version: "0.0.1",
	Use:     "unblisher",
	Short:   "A tool for downloading documents from Youblisher in PDF format.",
	Args:    cobra.MinimumNArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		log.Println("Initializing document...")
		doc, err := core.NewDocument(args[0], args[1])
		if err != nil {
			log.Panicf("error initializing document: %v", err)
		}

		log.Printf("Downloading images (total: %d)...", len(doc.Images))
		for i, img := range doc.Images {
			log.Printf("Downloading image %d of %d: %s", i+1, len(doc.Images), img.Name)
			if err = img.Download(); err != nil {
				log.Panicf("error downloading image %s: %v", img.Name, err)
			}
		}

		log.Printf("Generating PDF to %s...", doc.OutputPath)
		if err = doc.GeneratePDF(); err != nil {
			log.Panicf("error generating PDF: %v", err)
		}
	},
}

func Execute() {
	if err := unblisherCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

{{< / highlight >}}

##### `unblisher/main.go`

{{< highlight go "linenos=table" >}}
package main

import "gitlab.com/und0ck3d/unblisher/cmd"

func main() {
	cmd.Execute()
}
{{< / highlight >}}

## Usage

{{< highlight bash >}}
$ go run main.go http://www.youblisher.com/p/1947807-La-Bustia-edicio-maig-2018/
{{< / highlight >}}

## Final Notes

The full source code is available on [GitLab](https://gitlab.com/und0ck3d/unblisher).

This post will keep being updated with code and documentation improvements.