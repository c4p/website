+++
title = "Who we are?"
slug = "about"
+++

`c4p.studio` is a digital studio focused on free and open source software.

### Our Mission

* Contribute to the community by developing transparent solutions that somehow add value or contributing to existing ones;
* Promote the use of free software in people and organizations daily life.

### Our Values

* Freedom and tolerance are important;
* Sharing is good and empowers people;
* Security and privacy are a design concern, not a compliance one.